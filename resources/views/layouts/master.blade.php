<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <!-- Ionicons -->
  {{-- <link rel="stylesheet" href="{{ URL::asset('css/ionicons.min.css') }}" /> --}}
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('css/adminlte.min.css') }}" />
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ URL::asset('css/blue.css') }}" />
  <!-- Morris chart -->
  <link rel="stylesheet" href="{{ URL::asset('css/morris.css') }}" />
  <!-- jvectormap -->
  <link rel="stylesheet" href="{{ URL::asset('css/jquery-jvectormap.css') }}" />
  <!-- Date Picker -->
  <link rel="stylesheet" href="{{ URL::asset('css/jquery-jvectormap.css') }}" />
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ URL::asset('css/date-picker/daterangepicker-bs3.css') }}" />
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="{{ URL::asset('css/bootstrap3-wysihtml5.min.css') }}" />
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  @include('includes/navbar')
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  @include('includes/aside')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        @include('includes.boxes')
        <!-- /.row -->
        <!-- Main row -->
        @yield('content')
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2018 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.0-alpha
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script type="text/javascript" src="{{ URL::asset('js/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script type="text/javascript" src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script type="text/javascript" src="{{ URL::asset('js/bootstrap.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-ui.min.js') }}"></script>
<!-- Morris.js charts -->
<script type="text/javascript" src="{{ URL::asset('js/raphael.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script type="text/javascript" src="{{ URL::asset('js/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script type="text/javascript" src="{{ URL::asset('js/jquery-jvectormap.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script type="text/javascript" src="{{ URL::asset('js/jquery.knob.js') }}"></script>
<!-- daterangepicker -->
<script type="text/javascript" src="{{ URL::asset('js/jquery.knob.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datepicker/daterangepicker.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/datepicker/bootstrap-datepicker.js') }}"></script>
<!-- datepicker -->
<!-- Bootstrap WYSIHTML5 -->
<script type="text/javascript" src="{{ URL::asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script type="text/javascript" src="{{ URL::asset('js/bootstrap3-wysihtml5.all.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script type="text/javascript" src="{{ URL::asset('js/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="{{ URL::asset('js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script type="text/javascript" src="{{ URL::asset('js/dashboard.js') }}"></script>
<!-- AdminLTE for demo purposes -->
<script type="text/javascript" src="{{ URL::asset('js/demo.js') }}"></script>
</body>
</html>
